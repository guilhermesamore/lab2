public class BikeStore {

    public static void main(String[] args) {
        Bicycle bicycles[] = {
                new Bicycle("Monark", 21, 50),
                new Bicycle("Caloi", 7, 35),
                new Bicycle("Supreme", 50, 80),
                new Bicycle("Monark", 1, 40),
        };

        for (Bicycle bike : bicycles) {
            System.out.println(bike + "\n");
        }
    }
}
