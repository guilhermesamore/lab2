// Guilherme Machado de Souza e Correa. 2035536
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
       this.manufacturer = manufacturer;
       this.numberGears = numberGears;
       this.maxSpeed = maxSpeed;
    }

    public String getManufacter() {
        return manufacturer;
    }

    public int getNumberGears() {
        return numberGears;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        return  "Manufacturer: " + this.manufacturer + "\n" +
                "Number of Gears: " + this.numberGears + "\n" +
                "MaxSpeed: " + this.maxSpeed;
    }

}
